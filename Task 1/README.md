# Technical Test Backend Evermos

## Task 1

1. Dari studi kasus yang diberikan, masalah berasalah dari tidak updatenya data stok tiap produk yang membuat pelanggan tetap bisa melakukan order walaupun stok produk sudah habis dan dari sisi backend tidak ada pengecekan stok barang. seharusnya saat jumlah stok produk 0, maka produk tidak bisa diproses agar stok tidak menjadi minus.

2. Untuk menyelesaikan masalah diatas, data stok produk seharusnya diupdate saat pelanggan melihat detail produk dan saat akan melakukan checkout produk dari chart. Produk yang stok nya habis tidak bisa dichekout berdasarkan parameter stock_available=false yang didapat dari response api. Stok produk akan berkurang saat pelanggan sudah melakukan checkout dan menunggu pembayaran, proses pembayaran setiap pelanggan diberi waktu maksimal selama satu jam untuk melakukan pembayaran, agar jumlah produk bisa diupdate lagi sesuai dengan jumlah produk yang dichekout dan status transaksi menjadi dibatalkan jika sudah lewat dari satu jam. Produk juga tidak akan bisa ditambahkan kedalam chart ketika stok produk 0 atau kurang dari jumlah produk yang ingin dipesan pelanggan.

3. Collections API bisa tes dan diakses melalui https://www.postman.com/cloudy-meteor-942175/workspace/evermos-test
- Gunakan Environtment Live untuk mengakses online
- Gunakan Environtment Dev untuk mengakses project di local

## Setup project on local
- simpan project di htdocs
- Buat database mysql
- Import database menggunakan file evermos-test.sql
- Setting koneksi database di application/config/database.php
- Tes api menggunakan collections yang sudah dibuat dan set environment ke dev atau sesuaikan dengan path project local di pc anda
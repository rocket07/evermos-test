<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends BD_Controller {

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->auth();
    }

    public function index_get()
    {
        if (isset($this->user_data)) {
            $user_id = $this->get('user_id');
            $transaction_id = $this->get('transaction_id');
            if ($user_id) {
                if ($transaction_id) {
                    $this->db->where('id', $transaction_id);                    
                }
                $this->db->where('user_id', $user_id);
                $data = $this->db->get('transaction')->result();
                if ($data) {
                    $this->response(array('message' => 'Success get datas', 'data' => $data), 200);
                } else {
                    $this->response(array('message' => 'Data not found!'), 404);
                }
            }else{
                $this->response(array('message' => 'Please put user_id on parameters!'), 422);
            }
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }
    }

}

/* End of file Transaction.php */

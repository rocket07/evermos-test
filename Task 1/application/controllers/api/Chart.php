<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

class Chart extends BD_Controller {


    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->auth();
    }

    public function index_get($id_user = NULL)
    {
        if (isset($this->user_data)) {
            if ($id_user) {
                $this->db->where('id_user', $id_user);
                $data = $this->db->get('chart')->result();
                $result = [];
                foreach ($data as $a) {
                    $check_stock = $this->db->get_where('product', array('id' => $a->id_product))->row();
                    if ($check_stock->qty >= $a->qty) {
                        $insert = array(
                            'id' => $a->id,
                            'id_product' => $a->id_product,
                            'name' => $check_stock->name,
                            'image' => base_url() . 'uploads/product/' . $check_stock->image,
                            'qty' => $a->qty,
                            'price' => $check_stock->price,
                            'stock_available' => true,
                            'stock' => $check_stock->qty
                        );
                    } else {
                        $insert = array(
                            'id' => $a->id,
                            'id_product' => $a->id_product,
                            'name' => $check_stock->name,
                            'image' => base_url() . 'uploads/product/' . $check_stock->image,
                            'qty' => $a->qty,
                            'price' => $check_stock->price,
                            'stock_available' => false,
                            'stock' => $check_stock->qty
                        );
                    }
                    array_push($result, $insert);
                }

                if ($data) {
                    $this->response(array('message' => 'Success get datas', 'data' => $result), 200);
                } else {
                    $this->response(array('message' => 'Data not found!'), 404);
                }
            }else{
                $this->response(array('message' => 'Please put id_user on parameter!'), 422);
            }
            
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }
        
    }

    public function index_post()
    {
        if (isset($this->user_data)) {
            // Update Stock
            $this->refresh_stock();
            
            $id_product = $this->input->post('id_product');
            $qty = $this->input->post('qty');
            $id_user = $this->input->post('id_user');
            
            $check_stock = $this->db->get_where('product', array('id' => $id_product))->row();
            $this->response($check_stock);

            if ($check_stock->qty >= $qty) {
                $check_chart = $this->db->get_where('chart', array('id_user' => $id_user, 'id_product'=>$id_product))->row();
                if ($check_chart) {
                    $new_qty = $check_chart->qty + $qty;
                    $this->db->where('id_user' , $id_user);
                    $this->db->where('id_product' , $id_product);     
                    $insert = $this->db->update('chart', array('qty' => $new_qty));
                    $data = $this->db->get_where('chart', array('id_user' => $id_user, 'id_product' => $id_product))->row();
                } else {
                    $data = array(
                        'id_user' => $id_user,
                        'id_product' => $id_product,
                        'qty' => $qty
                    );
                    $insert = $this->db->insert('chart', $data);
                }

                if ($insert) {
                    $this->response(array('message' => 'Data created successfuly', 'data' => $data), 200);
                } else {
                    $this->response(array('message' => 'Failed create post, please check your data'), 422);
                }
            }else{
                $this->response(array('message' => 'Product is out of stock!'), 422);
            }
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }
    }

    public function index_put($id = null)
    {
        if (isset($this->user_data)) {
            if ($id) {
                $this->db->where('id', $id);
                $update = $this->db->update('chart', array('qty' => $this->put('qty')));
                $data = $this->db->get_where('chart', array('id' => $id))->row();                
                if ($update) {
                    $this->response(array('message' => 'Data updated successfuly', 'data' => $data), 200);
                } else {
                    $this->response(array('message' => 'Failed updated post'), 422);
                }                
            } else {
                $this->response(array('message' => 'Please put id chart on parameter!'), 422);
            }
            
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }        
    }

    public function index_delete($id = NULL)
    {
        if (isset($this->user_data)) {
            if ($id) {
                $this->db->where('id', $id);
                $del = $this->db->delete('chart');
                if ($del) {
                    $this->response(array('message' => 'Data deleted successfuly'), 200);
                } else {
                    $this->response(array('message' => 'Failed delete post'), 422);
                }
                
            } else {
                $this->response(array('message' => 'Please put id chart on parameter!'), 422);
            }
        } else {
            $this->response(array('message' => 'Bearer Token Not Defined!'), 422);
        }
    }

    public function refresh_stock()
    {
        $this->db->reset_query();
        date_default_timezone_set("Asia/Jakarta");
        $this->db->where('payment_deadline <', date('Ymdhis'));
        $this->db->where('payment_status', 'Pending');
        $cek = $this->db->get('transaction')->result();

        if ($cek) {
            foreach ($cek as $a) {
                $check_transaction = $this->db->get_where('transaction_detail', array('transaction_id' => $a->id))->result();
                if ($check_transaction) {
                    foreach ($check_transaction as $b) {
                        $check_stock = $this->db->get_where('product', array('id' => $b->product_id))->row();
                        $stock = $check_stock->qty + $b->qty;
                        $this->db->where('id', $b->product_id);
                        $this->db->update('product', array('qty' => $stock));
                    }
                }
                $this->db->where('id', $a->id);
                $this->db->update('transaction', array('payment_status' => 'Failed'));
            }
        }
    }

}

/* End of file Chart.php */

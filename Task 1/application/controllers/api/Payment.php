<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

class Payment extends BD_Controller {

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    public function index_post()
    {
        $transaction_id = $this->input->post('transaction_id');
        $payment_status = $this->input->post('payment_status');
        $this->db->where('id', $transaction_id);
        $this->db->update('transaction', array('payment_status'=>$payment_status));
        $this->response(array('message' => 'Success update payment status!'), 200);      
    }

}

/* End of file Payment.php */

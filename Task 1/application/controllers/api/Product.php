<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

class Product extends BD_Controller
{


    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->library('upload');
        $this->auth();
    }


    public function index_get($id = null)
    {
        if ($id) {
            // Update Stock
            $this->refresh_stock();
            // Where condition
            $this->db->where('id', $id);
        }
        $data = $this->db->get('product')->result();
        $result =[];
        foreach ($data as $a) {
            $insert = array(
                'id' => $a->id,
                'name' => $a->name,
                'price' => $a->price,
                'description' => $a->description,
                'image'=> base_url().'uploads/product/'.$a->image,
                'price' => $a->price,
                'qty' => $a->qty
            );
            array_push($result, $insert);
        }

        if ($data) {
            $this->response(array('message'=>'Success get datas', 'data' => $result), 200);
        } else {
            $this->response(array('message' => 'Data not found!'), 404);
        }
    }

    public function index_post()
    {
        if (isset($this->user_data)) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;
            // $config['max_size'] = 2000;

            $this->upload->initialize($config);
            if (!empty($_FILES['image']['name'])) {
                if ($this->upload->do_upload('image')) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './uploads/product/' . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['quality'] = '60%';
                    $config['width'] = 710;
                    $config['height'] = 420;
                    $config['new_image'] = './uploads/product/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    chmod('./uploads/product/' . $gbr['file_name'], 0777);
                    $gambar = $gbr['file_name'];
                    $data = array(
                        'name' => $this->post('name'),
                        'price' => $this->post('price'),
                        'description' => $this->post('description'),
                        'image' => $gambar,
                        'qty' => $this->post('qty')
                    );

                    $insert = $this->db->insert('product', $data);
                    if ($insert) {
                        $this->response(array('message'=>'Data created successfuly', 'data' => $data), 200);
                    } else {
                        $this->response(array('message' => 'Failed create post'), 422);
                    }

                    // $this->m_product->simpan_product($jdl,$product,$gambar);
                    // redirect('post_product/lists');
                } else {
                    $error =  $this->upload->display_errors();
                    $this->response(array('error' => $error), 422);
                }
            } else {
                $this->response(array('message' => 'image not found'), 404);
            }
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }
    }

    public function update_post($id = NULL)
    {
        if (isset($this->user_data)) {
            if ($id) {
                $config['upload_path'] = './uploads/product/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                // $config['max_size'] = 2000;

                $this->upload->initialize($config);
                if (!empty($_FILES['image']['name'])) {
                    if ($this->upload->do_upload('image')) {

                        $this->db->where('id', $id);
                        $cek = $this->db->get('product')->row();
                        unlink('uploads/product/' . $cek->image);
                        $this->db->reset_query();

                        $gbr = $this->upload->data();
                        //Compress Image
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './uploads/product/' . $gbr['file_name'];
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = FALSE;
                        $config['quality'] = '60%';
                        $config['width'] = 710;
                        $config['height'] = 420;
                        $config['new_image'] = './uploads/product/' . $gbr['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

                        $gambar = $gbr['file_name'];
                        $data = array(
                            'name' => $this->post('name'),
                            'price' => $this->post('price'),
                            'description' => $this->post('description'),
                            'image' => $gambar,
                            'qty' => $this->post('qty')
                        );
                        $this->db->where('id', $id);
                        $insert = $this->db->update('product', $data);
                        if ($insert) {
                            $this->response(array('message' => 'Data updated successfuly', 'data' => $data), 200);
                        } else {
                            $this->response(array('message' => 'Failed updated post'), 422);
                        }
                    } else {
                        $error =  $this->upload->display_errors();
                        $this->response(array('error' => $error), 402);
                    }
                } else {
                    $data = array(
                        'name' => $this->post('name'),
                        'price' => $this->post('price'),
                        'description' => $this->post('description'),
                        'qty' => $this->post('qty')
                    );
                    $this->db->where('id', $id);
                    $insert = $this->db->update('product', $data);
                    if ($insert) {
                        $this->response(array('message' => 'Data updated successfuly', 'data' => $data), 200);
                    } else {
                        $this->response(array('message' => 'Failed updated post'), 422);
                    }
                }
            } else {
                $this->response(array('message' => 'Please put id product on parameter!'), 422);
            }            
            
        } else {
            $this->response(array('message' => 'Bearer Token Not Defined!'), 422);
        }
    }

    public function index_delete($id = NULL)
    {
        if (isset($this->user_data)) {
            if ($id) {
                $this->db->where('id', $id);
                $cek = $this->db->get('product')->row();
                unlink('uploads/product/' . $cek->image);
                $this->db->reset_query();
                $this->db->where('id', $id);
                $del = $this->db->delete('product');
                if ($del) {
                    $this->response(array('message' => 'Data deleted successfuly'), 200);
                } else {
                    $this->response(array('message' => 'Failed delete post'), 422);
                }
            } else {
                $this->response(array('message' => 'Please put id product on parameter!'), 422);
            }            
            
        } else {
            $this->response(array('message' => 'Bearer Token Not Defined!'), 422);
        }
    }

    public function refresh_stock()
    {
        $this->db->reset_query();
        date_default_timezone_set("Asia/Jakarta");
        $this->db->where('payment_deadline <', date('Ymdhis'));
        $this->db->where('payment_status', 'Pending');
        $cek = $this->db->get('transaction')->result();

        if ($cek) {
            foreach ($cek as $a) {
                $check_transaction = $this->db->get_where('transaction_detail', array('transaction_id' => $a->id))->result();
                if ($check_transaction) {
                    foreach ($check_transaction as $b) {
                        $check_stock = $this->db->get_where('product', array('id' => $b->product_id))->row();
                        $stock = $check_stock->qty + $b->qty;
                        $this->db->where('id', $b->product_id);
                        $this->db->update('product', array('qty' => $stock));
                    }
                }
                $this->db->where('id', $a->id);                
                $this->db->update('transaction', array('payment_status' => 'Failed'));
            }
            
        }
    }
}

/* End of file Controllername.php */

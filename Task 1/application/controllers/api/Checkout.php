<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

class Checkout extends BD_Controller {

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->auth();
    }

    public function index_post()
    {
        if (isset($this->user_data)) {
            $id_user = $this->input->post('id_user');
            if ($id_user) {
                $check_chart = $this->db->get_where('chart', array('id_user' => $id_user))->result();
                $outstock_list = [];
                $stock_list = [];
                foreach ($check_chart as $a) {
                    $check_stock = $this->db->get_where('product', array('id' => $a->id_product))->row();
                    if ($check_stock->qty >= $a->qty) {
                        $insert = array(
                            'id' => $a->id,
                            'id_product' => $a->id_product,
                            'name' => $check_stock->name,
                            'image' => base_url() . 'uploads/product/' . $check_stock->image,
                            'qty' => $a->qty,
                            'price' => $check_stock->price,
                            'stock_available' => true,
                            'stock' => $check_stock->qty
                        );
                        array_push($stock_list, $insert);
                    } else {
                        $insert = array(
                            'id' => $a->id,
                            'id_product' => $a->id_product,
                            'name' => $check_stock->name,
                            'image' => base_url() . 'uploads/product/' . $check_stock->image,
                            'qty' => $a->qty,
                            'price' => $check_stock->price,
                            'stock_available' => false,
                            'stock' => $check_stock->qty
                        );
                        array_push($outstock_list, $insert);
                    }
                }

                if (sizeof($outstock_list)>0) {
                    $this->response(array('message' => 'Product out of stock found!', 'data' => $outstock_list), 422);
                }else {

                    // Setup data
                    $total_price=0;
                    date_default_timezone_set("Asia/Jakarta");
                    $payment_deadline = date('Ymdhis') + 10000;
                    foreach ($stock_list as $a) {
                        $price = $a['price']*$a['qty'];
                        $total_price= $total_price + $price;
                    }
                    $order_id = 'ordid-' . date('Ymdhis') . '-' . $id_user;

                    // Data use for store to the table
                    $data = array(
                        'order_id' => $order_id,
                        'user_id' => $id_user,
                        'payment_status' => 'Pending',
                        'payment_deadline' => $payment_deadline,
                        'total_price' => $total_price,
                        'shipping_cost' => 12000,
                        'total_bill' => $total_price - 12000
                    );
                    
                    // Data use for show on the response
                    $checkout_detail = array(
                        'order_id' => $order_id,
                        'user_id' => $id_user,
                        'payment_status' => 'Pending',
                        'payment_deadline' => date('d-M-Y h:i:s', strtotime($payment_deadline)),
                        'total_price' => $total_price,
                        'shipping_cost' => 12000,
                        'total_bill' => $total_price - 12000
                    );
                    $insert = $this->db->insert('transaction', $data);
                    $insert_id = $this->db->insert_id();                    
                    if ($insert) {
                        //Update stock product
                        foreach ($stock_list as $a) {
                            $stock = $a['stock']-$a['qty'];
                            $this->db->where('id', $a['id_product']);                            
                            $this->db->update('product', array('qty'=>$stock));

                            // Insert data to transaction details
                            $this->db->reset_query();
                            $data = array(
                                'transaction_id' => $insert_id,
                                'product_id' => $a['id_product'],
                                'qty' => $a['qty']
                            );
                            $this->db->insert('transaction_detail', $data);

                            // Deleting chart
                            $this->db->reset_query();
                            $this->db->where('id', $a['id']);
                            $this->db->delete('chart');

                            $this->response(array('message' => 'Checkout successfully!', 'checkout_detail' => $checkout_detail, 'products' => $stock_list), 200);
                            
                        }
                    } else {
                        $this->response(array('message' => 'Failed chechkout', 'error'=> $this->db->error()), 422);
                    }
                }
        
            } else {
                $this->response(array('message' => 'Please put id_user on form data!'), 422);
            }
            
            
        } else {
            $this->response(array('message' => array('message' => 'Bearer Token Not Defined!')), 422);
        }
    }

}

/* End of file Checkout.php */

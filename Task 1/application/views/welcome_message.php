<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to CodeIgniter</title>

    <style type="text/css">

    ::selection { background-color: #E13300; color: white; }
    ::-moz-selection { background-color: #E13300; color: white; }

    body {
        background-color: #FFF;
        margin: 40px;
        font: 16px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
        word-wrap: break-word;
    }

    a {
        color: #003399;
        background-color: transparent;
        font-weight: normal;
    }

    h1 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 24px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 15px;
    }

    code {
        font-family: Consolas, Monaco, Courier New, Courier, monospace;
        font-size: 16px;
        background-color: #f9f9f9;
        border: 1px solid #D0D0D0;
        color: #002166;
        display: block;
        margin: 14px 0 14px 0;
        padding: 12px 10px 12px 10px;
    }

    #body {
        margin: 0 15px 0 15px;
    }

    p.footer {
        text-align: right;
        font-size: 16px;
        border-top: 1px solid #D0D0D0;
        line-height: 32px;
        padding: 0 10px 0 10px;
        margin: 20px 0 0 0;
    }

    #container {
        margin: 10px;
        border: 1px solid #D0D0D0;
        box-shadow: 0 0 8px #D0D0D0;
    }
    </style>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() { 
        $("#formMhs").submit(function(e) {
            e.preventDefault();
            var file_data = $('#foto').prop('files')[0];
            var form_data = new FormData();
            var judul = $('#judul').val();

            form_data.append('foto', file_data);
            form_data.append('judul', judul);
            $.ajax({
                url: 'https://muba.socketspace.com/api/galeri_foto',
                // url: 'http://localhost/muba/api/galeri_foto',
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                async: true,
                crossDomain: true,
                type: 'post',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImRvZGlAZ21haWwuY29tIiwiaWF0IjoxNjI3Mzc5OTk5LCJleHAiOjE2MjczOTc5OTl9.X3IoHQgSrBX-2NvERN0QQYqneyz6RvjRUs7xHcBya8k'},
                data: form_data,             
                success: function(data) {      
                console.log(data);         
                // document.getElementById("formMhs").reset();
                // $('#status').html(data);              
                }
            });
        });
    })
    </script>
</head>
<body>

<div id="container">
    <h1>Welcome to CodeIgniter!</h1>

    <div id="body">

        <form id="formMhs" method="POST">
            <table>
            <!-- <tr>
            <td>email</td>
            <td><input type="text" name="email" id="email"></td>
            </tr>
            <tr>
            <td>password</td>
            <td><input type="password" name="password" id="password"></td>
            </tr> -->
            <tr>
                <td>judul</td>
                <td><input type="text" id="judul" name="judul"></td>
            </tr>
            <tr>
                <td>foto</td>
                <td><input type="file" id="foto" name="foto"></td>
            </tr>
            <tr>
            <td></td>
            <td>
                <input type="submit" name="simpan" id="simpan" value="Simpan">
            </td>
            </tr>
            </table>
        </form>
    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>'.CI_VERSION.'</strong>' : '' ?></p>
</div>

</body>
</html>

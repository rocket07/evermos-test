
$( document ).ready(function() {
    console.log('Testing API');
    $('#nama-admin').append(`
        <label class="text-muted" for="input-judul">Author</label>
        <input type="text" placeholder="Author" class="form-control" id="input-author" value="${namaAdmin}" disabled>
    `);

    $('#id-admin').append(`
        <label class="text-muted" for="input-admin">Admin Id</label>
        <input type="text" placeholder="Author" class="form-control" id="input-admin" value="${idAdmin}" disabled>
    `)
    $("#editor-btn").click(function (event) {
        var data = CKEDITOR.instances.editor.getData();
        alert(data)
    })

    // GET 
    $.ajax({
        type: "GET",
        url: api_url + "berita",
        async : true,
        dataType: "json",
        encode: true,
        success: function(data){
            event.preventDefault();
            console.log(data);
            console.log(data.length);
            var url = new URL(window.location.href);
            for(i = 0; i < data.length; i++){
                var dateTime = data[i].tanggal;
                var newDate = new Date(dateTime);
                if (data[i].premium == 0){
                    data[i].premium = 'Regular'
                } else {
                    data[i].premium = 'Premium'
                }
                $("#data-berita").append(`
                <tr>
                    <td>${data[i].id}</td>
                    <td><img src="https://muba.socketspace.com/uploads/berita/${data[i].foto}" class="w-50"></td>
                    <td>${data[i].judul}</td>
                    <td class="text-capitalize">${newDate.getDate()+ " " + monthNames[newDate.getMonth()]  + " " + newDate.getFullYear()}</td>
                    <td>${data[i].premium}</td>
                    <td>${data[i].author}</td>
                    <td class="text-center">
                        <a href="${`https://`+url.hostname+`/cms/edit/cmsberitaedit.html?index=${i}`}"><i class="text-info far fa-edit"></i></a>
                        <a href="#"><i id="deleteAnggota${data[i].id}" data-id="${data[i].id}" class="text-danger far fa-trash-alt"></i></a>
                    </td>
                </tr>
                `)
            }
        },
    });

    // POST
    $("#upload").click(function (event) {
        console.log("CLICKED!")
        event.preventDefault();
        var thumbnailBerita = document.getElementById('input-gambar').files[0];
        console.log(thumbnailBerita)
        // var formData = new FormData(this)
        var formData = {
            judul: $("#input-judul").val(),
            isi: CKEDITOR.instances.editor.getData(),
            tanggal: $("#input-tanggal").val(),
            sumber_foto: $("#input-sumber-foto").val(),
            admin_id : $("#input-admin").val(),
            author: $("#input-author").val(),
            premium: $("#input-jenis").val(),
            foto : $("#input-gambar").val()
        };
        $.ajax({
            type: "POST",
            url: api_url + "berita",
            data: formData,
            contentType : 'multipart/form-data',
            crossDomain: true,
            headers:{
                'Access-Control-Allow-Origin': '*',
                "Authorization":"Bearer" +
                localStorage.getItem('my_token')},
            dataType: "json",
            encode: true,
            success: function(data){
                event.preventDefault();
                console.log(data);
                console.log(formData);
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Basic " + btoa(""));
            },
        })
    });
});


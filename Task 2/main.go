package main

import "fmt"

	var move string
	var x = [][]string{}
	var values = [][]string{}
	var col int
	var row int
	// var treasure = [3][5]int{}
	var treasure = [][]string{}


func main() {

	for i := 0; i < 6; i++ {
		row := []string{".",".",".",".",".",".",".","."}
		treasure = append(treasure, row)
	}
    
    row1 := []string{"#","#","#","#","#","#","#","#"}
    row2 := []string{"#",".",".",".",".",".",".","#"}
    row3 := []string{"#",".","#","#","#",".",".","#"}
    row4 := []string{"#",".",".",".","#",".","#","#"}
    row5 := []string{"#",".","#",".",".",".",".","#"}
    row6 := []string{"#","#","#","#","#","#","#","#"}
    values = append(values, row1)
    values = append(values, row2)
    values = append(values, row3)
    values = append(values, row4)
    values = append(values, row5)
    values = append(values, row6)

	//Set user position 
	values[4][1] = "X"

	// Set Treasure position
	treasure[3][5] = "O"

	for i := 0; i < 6; i++ {
		fmt.Println(values[i])	
		for j := 0; j < 6; j++ {
			
			if values[i][j] == "X" {
				row = i
				col = j
			}
		}
		if i==5 {
			fmt.Println("a. Up")
			fmt.Println("b. Right")
			fmt.Println("c. Down")
			fmt.Println("x. Exit game")
			fmt.Print("Enter your move: ")
			fmt.Scanln(&move)
			if move=="a" {
				if treasure[row-1][col] == "O" {					
					values[row-1][col] = "O"					
					values[row][col] = "."
					found()
					break		
				}else{
					up(row ,col)
					fmt.Println("================================================================")
					fmt.Println()
				}
				i=-1
			}else if move=="b"{
				if treasure[row][col+1] == "O" {					
					values[row][col+1] = "O"					
					values[row][col] = "."
					found()
					break		
				}else{
					right(row ,col)
					fmt.Println("================================================================")
					fmt.Println()
				}
				i=-1
			}else if move=="c"{
				if treasure[row+1][col] == "O" {					
					values[row+1][col] = "O"					
					values[row][col] = "."
					found()
					break
				}else{
					down(row ,col)
					fmt.Println("================================================================")
					fmt.Println()
				}
				i=-1
			}else if move=="x"{
				break
			}else{
				fmt.Println("######## Command not found! ##########")
				fmt.Println()
				i=-1
			}
		}
	}
			
	
}

func maps()  {
	for i := 0; i < 6; i++ {
		fmt.Println(values[i])	
		for j := 0; j < 6; j++ {
			
			if values[i][j] == "X" {
				row = i
				col = j
			}
		}
	}
}

func found() {
	fmt.Println()
	maps()
	fmt.Println("######## You found the treasure! ##########")
	fmt.Println()
}

func obstacle()  {
	fmt.Println()
	fmt.Println("######## You hit an obstacle! ##########")
}

func up(i int, j int) {
	if values[i-1][j]=="#" {
		obstacle()
	}else{
		values[i-1][j]="X"
		values[i][j]="."
	}
}

func right(i int, j int) {
	if values[i][j+1]=="#" {
		obstacle()
	}else{
		values[i][j+1]="X"
		values[i][j]="."
	}
}

func down(i int, j int) {
	if values[i+1][j]=="#" {
		obstacle()
	}else{
		values[i+1][j]="X"
		values[i][j]="."
	}
}